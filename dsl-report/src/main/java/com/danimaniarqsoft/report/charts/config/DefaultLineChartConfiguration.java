package com.danimaniarqsoft.report.charts.config;

import org.jfree.chart.JFreeChart;

/**
 * Default Chart Configuration used by JfreeChartDSL
 * 
 * @author Daniel Cortes Pichardo
 *
 */
public class DefaultLineChartConfiguration extends
		ChartConfigurationAdapter {

	@Override
	public void configChart(JFreeChart chart) {
		super.configChart(chart);
	}
}
