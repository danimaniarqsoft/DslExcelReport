package com.danimaniarqsoft.report.charts.dsl;

import org.jfree.data.category.CategoryDataset;

public class BarChartState extends
		AbstractDomainCodomainChartState<BarChartState, CategoryDataset> {

	public BarChartState(ChartDslContext context) {
		super(context);
	}
}
