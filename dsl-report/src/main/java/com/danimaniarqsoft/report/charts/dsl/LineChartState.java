package com.danimaniarqsoft.report.charts.dsl;

import org.jfree.data.category.CategoryDataset;

public class LineChartState extends
		AbstractDomainCodomainChartState<LineChartState, CategoryDataset> {

	public LineChartState(ChartDslContext context) {
		super(context);
	}
}
