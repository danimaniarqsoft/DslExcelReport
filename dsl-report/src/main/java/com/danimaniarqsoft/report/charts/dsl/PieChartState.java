package com.danimaniarqsoft.report.charts.dsl;

import org.jfree.data.general.PieDataset;

public class PieChartState extends
		AbstractDomainCodomainChartState<PieChartState, PieDataset> {

	public PieChartState(ChartDslContext context) {
		super(context);
	}
}
