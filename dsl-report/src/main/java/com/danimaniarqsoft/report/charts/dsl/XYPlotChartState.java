package com.danimaniarqsoft.report.charts.dsl;

import org.jfree.data.xy.XYDataset;

public class XYPlotChartState extends
		AbstractDomainCodomainChartState<XYPlotChartState, XYDataset> {
	public XYPlotChartState(ChartDslContext context) {
		super(context);
	}
}
